[_metadata_:url]:- "https://gitlab.com/open-data-knowledge-sharing/wiki/-/wikis/home"

Nätverkets (NOSAD) mål är att praktisk hjälpa och inspirera offentlig verksamhet att öka tillgängliggörandet och nyttjandet av öppna data. Alla kan **delta**, **dela** och **diskutera**.

## Snabblänkar

1. Workshop
    * [Kalendarium](#kalendarium)
    * [Ta del av mötens utkomst i efterhand](#ta-del-av-mötens-utkomst-i-efterhand)
    * [Nyhetsbrev](#nyhetsbrev)
4. [Diskussionsforum](#diskussionsforum)
5. [Omvärldsanalys och delat material](#omvärldsanalys)

## Varför detta nätverk?

Vinsterna är många; öppen innovation; kostnadseffektiv digitalisering; kompetensdelning; samverkan som ökar chansen till interoperabilitet mellan system och verksamheter.

Vi tror på att gå från Ego till Eko. Att öppet samarbeta kring vår information för att bättre kunna fullfölja vårt uppdrag. Våra kunder delas av andra aktörer inom offentlig förvaltning. Våra kunders livshändelser spänner över flertalet aktörer inom offentlig sektor, men också privat sektor. Genom att skapa förutsättningar att öppet dela information och teknik, skapas också möjligheter för innovation – inte genom att börja på ett blankt papper men genom att hämta inspiration, data och kunskap där den redan finns och förvaltas.

Därför vill vi med detta nätverk bjuda in till kunskapsdelning och samverkan kring hur användande och samutveckling av öppna data och tillika öppen källkod och öppna standarder kan bidra till dessa vinster. Vårt mål är att hålla en praktisk tyngdpunkt och hjälpa och inspirera myndigheter i hur de kan ta nästa steg för att öka påverkan och driva på nyttiggörandet av deras öppna data.

## Kalendarium

Nätverket träffas regelbundet via virtuella workshops, första tisdagen varje månad. Vi välkomnar kontinuerligt förslag på ämnen och presentationer

| Datum | Ämne | Anmälan |
|---|---|---|
| 2022-03-29 | Kompetensförsörjning och livslångt lärande | [Anmälan](https://www.goto10.se/event/kompetensforsorjning-och-livslangt-larande/) |
| 2022-04-05 | Öppna data för ett hållbart godstransportsystem | [Anmälan](https://www.goto10.se/event/nosad-natverkande-kring-oppna-data-och-oppen-kallkod-5/) |

Öppna data, öppen källkod och öppna standarder är den röda tråden för alla workshops.

## Ta del av mötens utkomst i efterhand

Inspelade workshops nedan. Tidigare agendor och presentationer [finns här](https://gitlab.com/open-data-knowledge-sharing/wiki/-/wikis/Digital-Workshopserie). *De första inspelningar länkar till youtube (no-cookie) men flyttas succesivt över till https://data.arbetsformedlingen.se/videos.*

| Datum | Ämne | Inspelning |
|---|--|--|
| 2021-09-01 | Öppna licenser och immaterialrätt | [Inspelning](https://www.youtube-nocookie.com/embed/by_h0dWljqY/)
| 2021-09-15 | Samverkan kring offentlig sektors öppna data-satsningar | [Inspelning](https://www.youtube-nocookie.com/embed/-agLPUX5i9Q/)
| 2021-10-06 | Introduktion till öppen programvara | [Inspelning](https://www.youtube-nocookie.com/embed/jxxiqFyZhDU/)
| 2021-10-13 | Utfrågning öppna datautredningen | [Inspelning](https://www.youtube-nocookie.com/embed/63l3-_WC7jc/)
| 2020-11-03 | Förvaltning och ansvar kring öppna data | [Inspelning](https://data.jobtechdev.se/videos/nosad-20201103-forvaltning-och-ansvar-3440x1440.mp4)
| 2020-12-08 | Hackathons och innovationstävlingar | [Inspelning](https://data.jobtechdev.se/videos/nosad-20201208-hackathons-1920x1080.mp4)
| 2021-01 12 | Policy för öppen programvara hos myndigheter | [Inspelning](https://data.jobtechdev.se/videos/nosad-20210112-policy-oppen-programvara-myndigheter-1920x1080.mp4)
| 2021-02-02 | API-nycklar, OpenAPI och Jupyter Notebooks - hur maximerar vi användandet av öppna data? | [Inspelning](https://www.youtube-nocookie.com/embed/sfWWPpTysjk/)
| 2021-03-02 | Samverkan kring öppen programvara inom offentlig sektor. Hur gör man i andra länder? | [Inspelning](https://www.youtube-nocookie.com/embed/UvtX4e_qRWY/)
| 2021-04-06 | Länkade data och wikidata | [Inspelning](https://www.youtube-nocookie.com/embed/fkgKoN0NkeM/)
| 2021-05-04 | Öppna standarder | [Inspelning](https://www.youtube-nocookie.com/embed/W1k12G4stdA/)
| 2021-06-02 | Öppen källkod som bas i den digitala allemansrätten | [Inspelning](https://www.youtube-nocookie.com/embed/t5S7fSPgWOc/)
| 2021-06-15 | Offentlighetsprincipen - från bråkstake till bästa kompis | [Inspelning](https://data.jobtechdev.se/videos/nosad-20210615-offentlighetsprincipen-2736x1744.mp4)
| 2021-09-07 | Samarbete kring öppen källkod och data inom sektorn för kollektivtrafik |
| 2021-10-04 | Kan vi möta svenska städers behov av digital infrastruktur via nordamerikanska lärdomar? | [Inspelning](https://www.youtube-nocookie.com/embed/qFA3nE_mE2U/)
| 2021-10-12 | API-Playbook | [Inspelning](https://www.youtube-nocookie.com/embed/CpaSiEjjqwU/)
| 2021-12-07 | e-Arkiv open Source | [Inspelning](https://www.youtube-nocookie.com/embed/3Rz7a_I0NvA/)
| 2022-01-11 | Retrospektiv NOSAD 2021 | [Inspelning](https://data.jobtechdev.se/videos/nosad-20220111-retrospektiv-1920x1030.mp4)
| 2022-02-01 | Open by default inom offentlig verksamhet | [Inspelning](https://data.jobtechdev.se/videos/nosad-20220201-open-by-default-3840x2160.mp4)
| 2022-03-01 | Hälsa och säkerhet vid val av öppen programvara | [Inspelning](https://data.jobtechdev.se/videos/nosad-20220301-halsa-sakerhet-oppen-programvara-2560x1440.mp4)

## Nyhetsbrev

[Maila](mailto:maria.dalhage@digg.se) för att anmäla eller avanmäla dig till nyhetsbrevet. Skriv gärna “Prenumerera på nosads nyhetsbrev” eller “Avprenumerera på nosads nyhetsbrev” i ämnesraden.

## Omvärldsanalys och delat material

Dela gärna med er av er kunskap som ni tror skulle gynna någon annan. Vi välkomnar förslag på nytt material eller kompletterings/ändringsförslag. Det få som arbetar med öppna data och öppen källkod inom varje offentlig organisation. Men sammantaget är vi många.

Till [delat material](https://gitlab.com/open-data-knowledge-sharing/wiki/-/wikis/Delat-material)

## Diskussionsforum

För att kunna fortsätta diskussionen även utanför våra träffar, gör ett inlägg på vårt fråge- och diskussionsforum.

[Till diskussionsforumet](https://community.dataportal.se/)  

## Kontakt

[Kontakta oss via mail](mailto:maria.dalhage@digg.se)

## Vem står bakom nätverket

* Initiativtagare är äppna data-plattformarna [Arbetsförmedlingens jobtech](https://jobtechdev.se/) och [Trafiklab](https://www.trafiklab.se/), tillsammans med [Lunds universitet](http://cs.lth.se/english/research/software-engineering/).
* Sker i samarbete med [DIGG](https://www.digg.se/), [Internetstiftelsen](https://internetstiftelsen.se/) och [SKR](https://skr.se/).
